const fs = require('fs');
const Discord = require("discord.js");
const { prefix , token } = require("./config.json");

const client = new Discord.Client();
client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./commands',)
      .filter(file => file.endsWith('.js'));

const commands = commandFiles
      .map(file => require(`./commands/${file}`));

for (const command of commands)
    client.commands.set(command.name, command);

const cooldowns = new Discord.Collection ();

client.once('ready', () => {
	console.log('Ready!');
});

function enoughArguments (message, command, args) {
    return !command.args || args.length;
}

client.on('message', message => {
    if (!message.content.startsWith(prefix) || message.author.bot) return;

    const args = message.content.slice(prefix.length).split(/ +/);
    const commandName = args.shift().toLowerCase();

    if(!client.commands.has(commandName)) return;

    const command = client.commands.get(commandName);

    if(!enoughArguments(message, command, args)){
	let reply = `You didn\'t provide any arguments !`;

	if (command.usage) {
	    reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
	}
	
	return message.reply(reply);
    }

    if (command.guildOnly && message.channel.type !== 'text') {
	return message.reply('I can\'t execute that command inside DMs!');
}


    try{
	command.execute(message, args);
    }catch(err){
	console.error(err);
	message.reply(`there was an error trying to execute that command : ${commandName} !`);
    }
});

client.login(token);
