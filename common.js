/**
 * Compare two strings without case sensitivity.
 * Also trims the two strings before comparison.
*/
function cmp(s1, s2) {
    return !s1.trim().localeCompare(s2.trim(),{sensitivity: 'base'});
}

module.exports = {
    checkAnswer(message, correct_answer, given_answer) {
	if(!cmp(correct_answer, given_answer)){
	    message.reply(`Nice try ${message.author},` +
			  `but the correct answer was : ${correct_answer} `);
	    return false;
	} else {
	    message.reply('Wow, really impressive !' +
			  ` Give ${message.author} a cookie !`);
	    return true;
	}
    },
    
    checkAnswerWithoutGivingCorrect(message, correct_answer, given_answer) {
	if(!cmp(correct_answer, given_answer)){
	    message.reply('Nice try, but that is not correct');
	    return false;
	} else {
	    message.reply(`Wow, really impressive ! Give ${message.author} a cookie !`);
	    return true;
	}
    },
};
