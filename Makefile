all: exec

start:
	npm start

build:
	npm install

lint:
	node_modules/jshint/bin/jshint --exclude node_modules/ .

dockerbuild:
	docker build -t bot .

clean:
	rm -rf *~ */*~
