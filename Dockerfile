FROM node:stretch

ENV main index.js

RUN apt-get update
RUN apt-get upgrade -y

RUN mkdir -p /myapp
WORKDIR /myapp
COPY . .

RUN npm install

RUN make lint

CMD make start
