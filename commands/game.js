const fetch = require('node-fetch');
const question = require('./question.js');
const common = require('../common.js');
const Discord = require('discord.js');
const config = require('../config.json');
const displayScore = require('./score.js').displayScore;
const thumbsupEmoji = '👍';
const gameEmoji = '🎲';
const endEmoji = '🏁';

function generateUrl(nbQuestions = 1) {
    return `https://opentdb.com/api.php?amount=${nbQuestions}&encode=url3986`;
}

function Qa (data) {
    this.question = decodeURIComponent(data.question);
    this.answer = decodeURIComponent(data.correct_answer);
    this.status = 'unanswered';
}

function Player (_user) {
    this.score = 0;
    this.user = _user;
}

function createPlayers (users) {
    return users.map(u => new Player(u));
}

function createQas (data) {
    return data.results.map(d => new Qa(d));
}

let game = {
    players: [],
    qas: [],
    setPlayers (_players) {
	this.players = _players;
    },
    setQas (_qas) {
	this.qas = _qas;
    },
    updateScore(player) {
	let p = this.players.find(p => p.user === player);
	++p.score;
    }
};

function resetGame () {
    game.players = [];
    game.qas = [];
}

function newScoreCommand (channel, client) {
    let scoreCommand = client.commands.get('score');
    scoreCommand.execute =
	(message, args) => scoreCommand.displayScore(channel, game.players);
}

function newAnswerCommand (message, client) {
    let answerCommand = client.commands.get('answer');
    answerCommand.usage = 'answer <question id> <your answer>';
    answerCommand.execute = (message, args) => {
	if(args.length < 2){
	    message.reply(`The correct usage is **${answerCommand.usage}**`);
	    return;
	}

	const firstArg = args.shift();
	const questionId = parseInt(firstArg);
	
	if(isNaN(questionId) || questionId > game.qas.length){
	    message.reply(`Not a correct question id : ${firstArg}`);
	    return;
	}
	
	const qa = game.qas[questionId];

	if(qa.status === 'answered'){
	    message.reply('This question has already been answered');
	    return;
	}

	const correctAnswer = qa.answer;
	if(common.checkAnswerWithoutGivingCorrect(message, correctAnswer, args.join(' '))){
	    qa.status = 'answered';
	    message.channel.send(`1 point for ${message.author} !`);
	    game.updateScore(message.author);
	}
    };
}

function greetingMessage (channel) {
    let data = [
	`Game starting with player${game.players.length > 1 ? 's' : ''}`
    ];
    for(let p of game.players) data.push(p.user.username);
    data.push(`You can give an answer with **${config.prefix}answer <question id> <your answer>**`);
    data.push(`You can display the score with **${config.prefix}score**`);
    data.join('\n');
    channel.send(data);
}

function askQuestions (channel) {
    let msg = [];
    let i = 0;
    for(let {question} of game.qas)
	msg.push(`${i++}/ ${question}`);

    msg.join('\n');
    channel.send(msg);
}

function startGame (channel) {
    greetingMessage (channel);
    askQuestions (channel);
}

function resetCommands (client) {
    let scoreCommand = client.commands.get('score');
    scoreCommand.execute = (message, args) => {};

    let answerCommand = client.commands.get('answer');
    answerCommand.execute = (message, args) => {};
}

function endGame (channel, client) {
    let winners = [];
    let max = 0;
    
    console.assert(game.players.length > 0);

    for(let p of game.players){
	if(p.score > max){
	    winners = [p];
	    max = p.score;
	}else if(p.score === max){
	    winners.push(p);
	}
    }

    console.assert(winners.length > 0);

    let msg = `Winner(s) of the game : ${winners.reduce((s, u) => s + u.user.username + ' ', '')}`;
    channel.send(msg);

    let answers = ['Answer(s) :'];
    let i = 0;
    for(let {answer} of game.qas)
	answers.push(`${i++}/ ${answer}`);

    answers.join('\n');
    channel.send(answers);
    displayScore(channel, game.players);
    resetCommands(client);
}

function vote(message, joinEmoji, startEmoji) {
    let msg = [
	`You can join the game by smashing ${thumbsupEmoji} on the comment above.`,
	`It will start when ${message.author} will react with ${gameEmoji}.`,
	`It will end when ${message.author} will react with ${endEmoji}.`,
    ];
    msg.join('\n');

    message.channel.send(msg);
    
    message.react(joinEmoji);
    message.react(startEmoji);
    message.react(endEmoji);
    
    const startFilter =
	  (reaction, user) =>
	  reaction.emoji.name === startEmoji && !user.bot && user.username === message.author.username;

    let startReactionCollector =
	new Discord.ReactionCollector(message, startFilter);
    
    startReactionCollector.on('collect', collected => {

	let users = message.reactions.get(joinEmoji).users.values();
	users = Array.from(users).filter(u => !u.bot);

	if(users.length === 0){
	    message.channel.send('Nobody joined the game !');
	    return;
	}

	const players = createPlayers(users);
	game.setPlayers(players);
	startGame (message.channel);
    });

    const endFilter =
	  (reaction, user) =>
	  reaction.emoji.name === endEmoji && !user.bot && user.username === message.author.username;

    let endReactionCollector =
	new Discord.ReactionCollector(message, endFilter);

    endReactionCollector.on('collect', collected =>
			    endGame(message.channel, message.client));
}



module.exports = {
    name: 'game',
    description: 'what about a good ol\' common knowledge game ?',
    usage: 'game [nb questions]',
    execute (message, args) {

	resetGame();

	let nbQuestions = args.length > 0 ? parseInt(args[0]) : 1;
	const channel = message.channel;
	const client = message.client;

	if(isNaN(nbQuestions) || nbQuestions < 1) nbQuestions = 1;
	
	const url = generateUrl(nbQuestions);
	fetch(url, { 'Content-Type': 'application/json' })
	    .then(res => res.json())
	    .then(data => createQas(data))
	    .then(qas => game.setQas(qas))
	    .then(() => vote(message, thumbsupEmoji, gameEmoji))
	    .catch(console.error);

	newAnswerCommand(message, client);
	newScoreCommand(channel, client);
    },
};
