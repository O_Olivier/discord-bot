const thumbsUpEmoji = '👍';
const thumbsDownEmoji = '👎';
const endPollEmoji = '🏁';

const Discord = require('discord.js');

module.exports = {
    name: 'poll',
    description: `react with ${thumbsUpEmoji} or ${thumbsDownEmoji}to vote, react with ${endPollEmoji} to close the poll`,
    usage: 'poll <question>',
    execute (message, args) {
	if(args.length === 0){
	    message.reply(this.usage);
	    return;
	}
	
	const emojiFilter =
	      (emoji) =>
	      (reaction, user) => reaction.emoji.name === emoji && !user.bot;	

	const endReactionCollector =
	      new Discord.ReactionCollector(message, emojiFilter(endPollEmoji));
	
	endReactionCollector.on('collect', collected => {
	    endReactionCollector.stop();
	    let nbThumbsUp = 0;
	    let nbThumbsDown = 0;

	    const notBot = user => !user.bot;
	    
	    for(let r of message.reactions.values()){
		if(r.emoji.name === thumbsUpEmoji)
		    nbThumbsUp = r.count;
		else if(r.emoji.name === thumbsDownEmoji)
		    nbThumbsDown = r.count;
	    }
	    message.channel.send(`${args.join(' ')} :  ${nbThumbsUp} ${thumbsUpEmoji}       ${nbThumbsDown} ${thumbsDownEmoji}`);
	});
    }
};
