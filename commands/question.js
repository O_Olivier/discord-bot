const fetch = require('node-fetch');
const common = require('../common.js');

const url = 'https://opentdb.com/api.php?amount=1&encode=url3986';

let state = 'asking';

function newAnswerCommand (client, correct_answer, answerCommand) {
    answerCommand.usage = 'answer <your answer>';

    answerCommand.execute = (message, args) => {
	if(args.length === 0) return;
	common.checkAnswer(message, correct_answer, args[0]);
	state = 'asking';
    };

    client.commands.set('answer', answerCommand);
}

module.exports = {
    name: 'question',
    description: 'ask a question',
    execute (message, args) {

	if(state == 'asking'){
	    fetch(url, {
		'Content-Type': 'application/json'
	    })
		.then(res => res.json())
		.then(out => {
		    let { question , correct_answer } = out.results[0];
		    message.channel.send(decodeURIComponent(question));
		    return decodeURIComponent(correct_answer);
		})
		.then(correct_answer => {
		    console.log(correct_answer);
		    state = 'waiting';
		    let answerCommand = message.client.commands.get('answer');
		    newAnswerCommand(message.client, correct_answer, answerCommand);
		})		  
		.catch(err => console.error(err));
	}

	else if (state == 'waiting') {
	    message.channel.send(`Slow down ${message.author},`+
				 'you must first answer my last question !');
	}

	else {
	    console.error(`Unknown state : ${state}`);
	}
    },
};
