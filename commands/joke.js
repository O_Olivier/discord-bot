const fetch = require("node-fetch");

module.exports = {
    name: 'joke',
    description: 'fetch a joke from the web',
    execute (message, args) {
	url = 'https://icanhazdadjoke.com/';
	fetch(url , {
	    method: 'GET',
	    headers: { Accept : 'application/json' },
	})
	    .then(res => res.json())	
	    .then(out => message.channel.send(out.joke))
	    .catch(err => console.error(err));
    },
};
