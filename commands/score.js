module.exports = {
    name: 'score',
    displayScore (channel, players) {
	let msg = players.map(
	    ({ score, user }) => `${user.username} : ${score} pts`
	);
	msg.join('\n');
	channel.send(msg);			     
    },
    execute (message, args) {},
};
