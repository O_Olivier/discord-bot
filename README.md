In order to run this bot, you need to create a file called 'config.json' in the root directory.
This file must look like this :

```json
{
	"token" : "YOUR-TOKEN-GOES-HERE",
	"prefix" : "!"
}
```

You can learn more about how to retrieve your bot token and how to set up a bot application for discord [here](https://discordjs.guide/preparations/setting-up-a-bot-application.html#creating-your-bot).

Please remember that you must **never** leak your token.
